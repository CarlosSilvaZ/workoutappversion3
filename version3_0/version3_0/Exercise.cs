﻿using System;
using System.Collections.Generic;

public class Exercise
{
    public String exerciseName { get; set; }
    public List<quantitExercise> quantExerciseData { get; } // Stores varying (exercise)set information
    public int numOfQuantExercisez;     // number of sets done for this exercise
    public List<CardioExercise> cardioExerciseData;    // Stores varying cardio exercises' information
    public int numOfCardioExercisez;    // number of cardio exercise done for this exercise // Will usually only be one
    public string muscleGroup { get; set; }

    // If nothing is passed into an new Exercise object
    public Exercise()
    {
        quantExerciseData = new List<quantitExercise>();
        cardioExerciseData = new List<CardioExercise>();
    }

    public Exercise(string name, string muscleGroup)
    {
        exerciseName = name;
        this.muscleGroup = muscleGroup;
        quantExerciseData = new List<quantitExercise>();
        cardioExerciseData = new List<CardioExercise>();
    }

    // Constructor for when an exercise name, number of repetitions
    // and weight lifted are passed into a new Exercise object
    public Exercise(string eName, string muscleGroup, int reps, double weight)
    {
        this.muscleGroup = muscleGroup;
        quantExerciseData = new List<quantitExercise>();
        quantExerciseData.Add(new quantitExercise(reps, weight));
        this.exerciseName = eName;
    }

    // Constructor for when an exercise name, duration of exercise
    // and weight lifted are passed into a new Exercise object
    public Exercise(String name, DateTime time, double weight)
    {
        cardioExerciseData = new List<CardioExercise>();
        cardioExerciseData.Add(new CardioExercise(time, weight));
    }

    // Adds a set to an exercise
    // Accepts number of reps and the weight done.
    public void addQuantExercise(int reps, double weight)
    {
        quantExerciseData.Add(new quantitExercise(reps, weight));
    }

    // Adds a(nother) cardio exercise time
    // maybe if its an exercise that has multiple intervals
    // Will usually only be an array of one
    public void addCardioExercise(DateTime time, double weight)
    {
        cardioExerciseData.Add(new CardioExercise(time, weight));
    }

    //public List<quantitExercise> returnSets()
    //{
    //    return 
    //}

    public override string ToString()
    {
        int index = 0;
        String toReturn = exerciseName + "\n";
        foreach (quantitExercise set in quantExerciseData)
        {
            toReturn += (index + 1 + ") ") + set.weight + " lbs x " +
                        set.repetitions + "\n";
            index++;
        }
        return toReturn;
    }

}
