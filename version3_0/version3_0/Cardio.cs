﻿using System;

// A CardioExercise object contains two data fields,
// an DateTime type and a double type.
public class CardioExercise
{
    public DateTime time { get; set; }
    public double weight { get; set; }

    public CardioExercise()
    {

    }

    public CardioExercise(DateTime time, double weight)
    {
        this.time = time;
        this.weight = weight;
    }
}
