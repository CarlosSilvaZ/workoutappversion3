﻿using System;
using System.Collections;

public class Test1
{ 
    public void runTest()
    {
        Console.WriteLine("What would you like to test? ");
        Console.WriteLine("1) Create exercise with one set \n" +
            "2) Create exercise with multiple sets\n" +
            "3) Create a single routine\n" + 
            "4) Create multiple routines");
        String userinput = Console.ReadLine();
        int answer = Convert.ToInt32(userinput);
        Console.WriteLine();
        switch(answer)
        {
            case 1:
                Exercise exercise1 = testOneInput();
                backupData(exercise1);
                break;
            case 2:
                Exercise exerciseMult = testMultiple();
                backupData(exerciseMult);
                break;
            case 3:
                Routine routine1 = testSingleRoutine();
                backupData(routine1);
                break;
            case 4:
                testMultipleRoutines();
                break;
        }

    }

    private void backupData(Routine routine)
    {
        Console.WriteLine("Press 'b' to backup data");
        string backup = Console.ReadLine();
        if (backup.Equals("b"))
        {
            ManageData m = new ManageData();
            m.fillRoutineTable(routine);
        }
    }

    private void backupData(Exercise exercise)
    {
        Console.WriteLine("Press 'b' to backup data");
        string backup = Console.ReadLine();
        if (backup.Equals("b"))
        {
            ManageData m = new ManageData();
            m.fillExerciseTable(exercise);
        }
    }

    private void testMultipleRoutines()
    {
        Routine legDay = new Routine("Leg Day");

        Exercise squat = new Exercise("Squat", "Legs");
        squat.addQuantExercise(8, 185);
        squat.addQuantExercise(8, 185);
        squat.addQuantExercise(8, 185);

        Exercise legPress = new Exercise("Leg Press", "Legs");
        legPress.addQuantExercise(12, 175);
        legPress.addQuantExercise(12, 185);
        legPress.addQuantExercise(12, 185);

        Exercise romanian = new Exercise("Romanian Deadlift", "Legs");
        romanian.addQuantExercise(12, 135);
        romanian.addQuantExercise(8, 155);
        romanian.addQuantExercise(8, 155);
        romanian.addQuantExercise(12, 135);

        legDay.addExercise(squat);
        legDay.addExercise(legPress);
        legDay.addExercise(romanian);

        /*********************************/

        Routine pushDay = new Routine("Push day");

        Exercise bp = new Exercise("Bench Press", "Chest");
        bp.addQuantExercise(10, 135);
        bp.addQuantExercise(10, 135);
        bp.addQuantExercise(10, 135);

        Exercise ibp = new Exercise("Incline Bench Press", "Chest");
        ibp.addQuantExercise(10, 115);
        ibp.addQuantExercise(10, 125);
        ibp.addQuantExercise(10, 125);

        Exercise lr = new Exercise("Lateral Raise", "Shoulders");
        lr.addQuantExercise(20, 17.5);
        lr.addQuantExercise(20, 17.5);
        lr.addQuantExercise(20, 17.5);
        lr.addQuantExercise(20, 17.5);

        Exercise tp = new Exercise("Tricep Pushdown", "Arms");
        tp.addQuantExercise(15, 110);
        tp.addQuantExercise(15, 110);
        tp.addQuantExercise(15, 110);

        pushDay.addExercise(bp);
        pushDay.addExercise(ibp);
        pushDay.addExercise(lr);
        pushDay.addExercise(tp);

        Console.WriteLine(legDay);
        Console.WriteLine(pushDay);

    }

    private Routine testSingleRoutine()
    {
        Routine pushDay = new Routine("Push day");

        Exercise bp = new Exercise("Bench Press", "Chest");
        bp.addQuantExercise(10, 135);
        bp.addQuantExercise(10, 135);
        bp.addQuantExercise(10, 135);

        Exercise ibp = new Exercise("Incline Bench Press", "Chest");
        ibp.addQuantExercise(10, 115);
        ibp.addQuantExercise(10, 125);
        ibp.addQuantExercise(10, 125);

        Exercise lr = new Exercise("Lateral Raise", "Shoulders");
        lr.addQuantExercise(20, 17.5);
        lr.addQuantExercise(20, 17.5);
        lr.addQuantExercise(20, 17.5);
        lr.addQuantExercise(20, 17.5);

        Exercise tp = new Exercise("Tricep Pushdown", "Arms");
        tp.addQuantExercise(15, 110);
        tp.addQuantExercise(15, 110);
        tp.addQuantExercise(15, 110);

        pushDay.addExercise(bp);
        pushDay.addExercise(ibp);
        pushDay.addExercise(lr);
        pushDay.addExercise(tp);
        Console.WriteLine(pushDay);
        return pushDay;
    }

    private Exercise testMultiple()
    {
        Console.WriteLine("How many inputs? ");
        string userInput = Console.ReadLine();
        int numOfInputs = Convert.ToInt32(userInput);
        Console.WriteLine();
        Exercise bp = new Exercise("Bench Press", "Chest");

        for (int i = 0; i < numOfInputs; i++)
        {
            bp.addQuantExercise(8, 225);
        }
        Console.WriteLine(bp);
        return bp;
    }

    private Exercise testOneInput()
    {
        Exercise squat = new Exercise("Squat", "Legs", 10, 200.0);
        Console.WriteLine(squat);
        return squat;
    }
}
