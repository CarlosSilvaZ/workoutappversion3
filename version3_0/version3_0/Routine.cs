﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Routine
{
	public string routineName { get; set; } // name of the routine
    public DateTime DateTime;   // date and time of workout
    public string notes;    // notes about the workout  TODO: Maybe change to txt file
    public Dictionary<string, Exercise> exerciseMap { get; set; } // "Exercise name": Exercise object
    public int numOfExercises;

    // Constructor with no inputs
    public Routine()
    {
        exerciseMap = new Dictionary<string, Exercise>();
    }

    // Constructor with name of routine as input
    public Routine(string name)
    {
        routineName = name;
        exerciseMap = new Dictionary<string, Exercise>();
    }

    // Constructor with routine name, date/time of exercise, notes, and 
    // premade exercise object
    public Routine(string name, DateTime dateTime, string notes, Exercise exercise)
    {
        routineName = name;
        this.DateTime = dateTime;
        this.notes = notes;
        this.exerciseMap.Add(exercise.exerciseName, exercise);
    }

    // Method adds an exercise
    // to the dictionary
    public void addExercise(Exercise exercise)
    {
        exerciseMap.Add(exercise.exerciseName, exercise);
    }

    // Prints routines
    public override string ToString()
    {
        String toReturn = "";
        toReturn += routineName.ToUpper() + "\n";
        Dictionary<string, Exercise>.ValueCollection values = exerciseMap.Values;
        foreach(Exercise exercise in values)
        {
            toReturn += exercise+ "\n";
        }

        return toReturn;
    }

    public int getNumOfExercise()
    {
        return exerciseMap.Count;
    }
}
