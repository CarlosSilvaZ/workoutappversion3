﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

public class ManageData
{
    SQLiteConnection m_dbConnection;


    public ManageData() 
    {
        connectToDatabase();
    }

    public void printHighscores()
    {
        string sql = "select * from highscores order by score desc";
        SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
        SQLiteDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
            Console.WriteLine("Name: " + reader["name"] + "\tScore: " + reader["score"]);
        }
        Console.ReadLine(); // So the program doesnt automatically shutdown
    }

    // Fills an entire routine
    public void fillRoutineTable(Routine routine)
    {
        // TODO: Add datetime and notes
        string routineEntrySQL = "insert into Routine (routineName, datetime, notes) values ('" + routine.routineName + "', '', '')";
        SQLiteCommand command = new SQLiteCommand(routineEntrySQL, m_dbConnection);
        command.ExecuteNonQuery();

        Dictionary<string, Exercise>.ValueCollection values = routine.exerciseMap.Values;
        foreach(Exercise exercise in values)
        {
            fillExerciseTable(exercise);
        }

        //*** Add to 'routine_exercise' table ***//
        // grab the recently added exercises
        int exerciseCount = routine.getNumOfExercise();
        string eSQL = "select * from Exercise order by id desc limit " + exerciseCount;
        SQLiteCommand esCommand = new SQLiteCommand(eSQL, m_dbConnection);
        SQLiteDataReader eReader = esCommand.ExecuteReader();

        // grab the recently added exercise
        string rSQL = "select * from Routine order by id desc limit 1";
        SQLiteCommand reCommand = new SQLiteCommand(rSQL, m_dbConnection);
        string rRederNum = reCommand.ExecuteScalar().ToString();
        int rReaderNum = Convert.ToInt32(rRederNum);

        // Add to 'routine_exercise' table
        while (eReader.Read())
        {
            string r_eEntrySQL = "insert into routine_exercise (routineID, exerciseID) values (" + rReaderNum + ", " + eReader["id"] + ")";
            SQLiteCommand e_sCommand = new SQLiteCommand(r_eEntrySQL, m_dbConnection);
            e_sCommand.ExecuteNonQuery();
        }
    }

    // Fills (probably edits) an exercise
    public void fillExerciseTable(Exercise exercise)
    {
        // Add to 'Exercise' SQL table
        string exerciseEntrySQL = "insert into Exercise (exerciseName, muscleGroup) values ('" + exercise.exerciseName + "', '" + exercise.muscleGroup + "')";
        SQLiteCommand eCommand = new SQLiteCommand(exerciseEntrySQL, m_dbConnection);
        eCommand.ExecuteNonQuery();

        // Add to 'Sets' SQL table
        int setCount = exercise.quantExerciseData.Count;
        for (int i = 0; i < setCount; i++)
        {
            quantitExercise s = exercise.quantExerciseData[i];
            string setEntrysql = "insert into Sets (repetitions, weightLifted) values (" + s.repetitions + ", " + s.weight + ")";
            SQLiteCommand sCommand = new SQLiteCommand(setEntrysql, m_dbConnection);
            sCommand.ExecuteNonQuery();
        }

        //*** Add to 'exercise_set' table ***//
        // grab the recently added sets
        string sSQL = "select * from Sets order by id desc limit " + setCount;
        SQLiteCommand esCommand = new SQLiteCommand(sSQL, m_dbConnection);
        SQLiteDataReader sReader = esCommand.ExecuteReader();

        // grab the recently added exercise
        string eSQL = "select * from Exercise order by id desc limit 1";
        SQLiteCommand es2Command = new SQLiteCommand(eSQL, m_dbConnection);
        string eRederNum = es2Command.ExecuteScalar().ToString();
        int eReaderNum = Convert.ToInt32(eRederNum);

        // Add to 'exercise_set' table
        while (sReader.Read())
        {
            string e_sEntrySQL = "insert into exercise_set (exerciseID, setID) values (" + eReaderNum + ", " + sReader["id"] + ")";
            SQLiteCommand command = new SQLiteCommand(e_sEntrySQL, m_dbConnection);
            command.ExecuteNonQuery();
        }
        
    }

    public void createTable()
    {
        string sql = "create table highscores (name varchar(20), score int)";
        SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
        command.ExecuteNonQuery();
    }

    public void connectToDatabase()
    {
        m_dbConnection = new SQLiteConnection("Data Source=MyDatabaseVersion2.sqlite;Version=3;");
        m_dbConnection.Open();
    }

    public void createNewDatabase()
    {
        SQLiteConnection.CreateFile("MyDatabaseVersion2.sqlite");

    }

    public bool tableExists()
    {
        string sql = "";
        SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
        return false;
    }

}
