﻿using System;


// A quantitExercise object contains two data fields,
// an int type and a double type.
public class quantitExercise
{
    public int repetitions { get; set; }
    public double weight { get; set; }

    public quantitExercise()
    {

    }

    public quantitExercise(int reps, double weight)
    {
        this.repetitions = reps;
        this.weight = weight;
    }

    public void addSet(int reps, double weight)
    {
        this.repetitions = reps;
        this.weight = weight;
    }
}
